var categories = {
    3 : {
         name: "Apparel & Accessories"
    },

    34 : {
        name: "Automobiles & Motorcycles"
    },

    1501 : {
        name: "Baby Products"
    },

    66 : {
        name: "Beauty & Health"
    },

    7 : {
        name: "Computer & Networking"
    },

    13 : {
        name: "Construction & Real Estate"
    },

    44 : {
        name: "Consumer Electronics"
    },

    100008578 : {
        name: "Customized Product"
    },

    5 : {
        name: "Electrical Equipment & Supplies"
    },


    502 : {
        name: "Electronic Components & Supplies"
    },

    2 : {
        name: "Food"
    },

    1503 : {
        name: "Furniture"
    },

    200003655 : {
        name: "Hair & Accessories"
    },

    42 : {
        name: "Hardware"
    },

    15 : {
        name: "Home & Garden"
    },

    6 : {
        name: "Home Appliances"
    },

    200003590 : {
        name: "Industry & Business"
    },

    36 : {
        name: "Jewelry & Watch"
    },

    39 : {
        name: "Lights & Lighting"
    },

    1524 : {
        name: "Luggage & Bags"
    },

    509 : {
        name: "Phones & Telecommunications"
    },

    30 : {
        name: "Security & Protection"
    },

    322 : {
        name: "Shoes"
    },

    200001075 : {
        name: "Special Category"
    },

    18 : {
        name: "Sports & Entertainment"
    },

    1420 : {
        name: "Tools"
    },

    26 : {
        name: "Toys & Hobbies"
    },

    1511 : {
        name: "Watches"
    },
};
module.exports = categories;