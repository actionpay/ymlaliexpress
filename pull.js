/**
 * Created by Yagubov Vladimir on 22.09.2016.
 */
var categories = require('./categories.js');
var dataCollector = require('./dataCollector.js');
var comfigManager = require('./configManager');
var currentConfig = comfigManager.getConfig();

exports.pullAll = function () {
    var url = 'http://gw.api.alibaba.com/openapi/param2/2/portals.open/api.listPromotionProduct/18026' +
        '?fields=totalResults,productId,productTitle,productUrl,imageUrl,originalPrice,salePrice,discount,evaluateScore,' +
        'commission,commissionRate,volume,packageType,lotNum,validTime,localPrice' +
        '&pageSize=40&highQualityItems=true';

    var ids = [];
    for (var id in categories) {
        (function(id){
            ids.push(id);
        })(id);
    }

    function pullRepeat(dataCollector) {
        var id = ids.pop();
        console.log(categories[id]);
        var category = {
            id: id,
            name: categories[id].name,
            url: categories[id].hasOwnProperty('url') ? categories[id].url : url
        };

        dataCollector.init(category, currentConfig);
        dataCollector.pull().then(function (data) {
            pullRepeat(dataCollector);
        });
    }

    pullRepeat(dataCollector);
};