/**
 * Created by yagubov on 14.09.2016.
 */

var request = require('request') // https://www.npmjs.com/package/request
    , async = require('async')
    , http = require('http')
    , fs = require('fs-extra')
    , dateformat = require('dateformat')
    , xmlescape = require('xml-escape')
    , Agent = require('agentkeepalive')
    ; // https://www.npmjs.com/package/async

var baseUrl = '';
var config = null;
var saveDataFile = null;
var logger = null;
var currentPage = 1;
var offerIds = [];
var currentCategory = null;
var keepAliveAgent = undefined;
var tempDataFileOptions = null;

function escape(xmlNode) {
    return xmlescape('' + xmlNode); // toString and escape
}

function getNextUrl(){
    return baseUrl + '&pageNo=' + currentPage++;
}

function parseFloat(value){
    var regex = /[0-9](\.[0-9]+)/g;
    return value.match(regex);
}

function saveData(data){
    return new Promise(function(resolve,reject){
        var xml = '';
        var saved = 0;

        for (index in data) {
            var row = data[index];
            var productId = row.productId;
            if (typeof offerIds[productId] == 'undefined') {
                xml += '<offer id="' + escape(row.productId) + '">'
                    + '<url>' + escape(row.productUrl) + '</url>'
                    + '<price>' + parseFloat(row.salePrice) + '</price>'
                    + '<discount>' + escape(row.discount)+ '</discount>'
                    + '<originalPrice>' + parseFloat(row.originalPrice) + '</originalPrice>'
                    + '<currencyId>USD</currencyId>'
                    + '<picture>' + escape(row.imageUrl) +  '</picture>'
                    + '<name>' + escape(row.productTitle) + '</name>'
                ;
                xml += '</offer>';
                offerIds[productId] = productId;
                saved++;
            }
        }
        saveDataFile.write(xml, function () {
            console.log('saved: ' + saved);
            resolve();
        })
    });
}

function makeRequest (index, callback) {
    return new Promise(function(accept, reject) {
        var nextUrl = getNextUrl();
        console.log(nextUrl);

        request.get(nextUrl, {agent : keepAliveAgent}, function (error, response, body) {
            console.log(index);
            console.log(body);
            if (body) {
                body = JSON.parse(body);
                if (body.hasOwnProperty('result') && body.result.products.length > 0) {
                    saveData(body.result.products).then(
                        function () {
                            makeRequest(index, callback);
                        }
                    )
                } else {
                    callback(null, 'ok');
                }
            } else {
                callback(null, 'no data');
            }
        });
    });
}

exports.init = function(category, currentConfig) {
    console.log(currentConfig);
    currentPage = 1;
    currentCategory = category;
    baseUrl = currentCategory.url + '&categoryId=' + currentCategory.id;

    config = currentConfig;
    var logFileOptions = (currentConfig.server.hasOwnProperty('logFileOptions'))
        ? currentConfig.server.logFileOptions
        : {
        flags: 'w',
        encoding: 'utf8',
        name: 'events.log'
    };
    keepAliveAgent = new Agent(config.server.keepAliveAgent);
    logger = fs.createWriteStream(__dirname + '/' + logFileOptions.name, logFileOptions);

    tempDataFileOptions = config.server.tempDataFileOptions;
    saveDataFile = fs.createWriteStream(__dirname + '/' + tempDataFileOptions.temp + '_' + currentCategory.id + '.yml', tempDataFileOptions);
};

exports.pull = function(){
    return new Promise(function (accept, reject) {
        var treads = [];

        for (var i = 1; i <= 4; i++) {
            (function (index) {
                treads.push(function (callback) {
                    makeRequest(index, callback);
                });
            })(i)
        }

        var now = new Date();
        logger.write('Start category=' + currentCategory.id + ' ' + dateformat(now, 'yyyy-mm-dd HH:MM:ss') + "\n");

        saveDataFile.write('<?xml version="1.0" encoding="utf-8"?>');
        saveDataFile.write('<yml_catalog date="' + dateformat(now, 'yyyy-mm-dd HH:MM:ss') + '"><shop>');

        var xml = '<name>Aliexpress</name>' +
                  '<company>Aliexpress</company>' +
                  '<url>http://aliexpress.com/</url>' +
                  '<currencies><currency id="USD" rate="80"/></currencies>' +
                  '<categories><category id="' + currentCategory.id + '">' + escape(currentCategory.name) + '</category></categories><offers>'; 

        saveDataFile.write(xml);

        async.parallel(treads, function (error, data) {
            saveDataFile.write('</offers></shop></yml_catalog>');
            logger.write('End category=' + currentCategory.id + ' ' + dateformat(new Date(), 'yyyy-mm-dd HH:MM:ss') + "\n");

            var tempName = __dirname + '/' + tempDataFileOptions.temp + '_' + currentCategory.id + '.yml';
            var originName = __dirname + '/' + tempDataFileOptions.origin + '_' +  currentCategory.id + '.yml';
            fs.copySync(tempName, originName);

            accept(data);
        });
    });
};