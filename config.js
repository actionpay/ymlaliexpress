exports.config = {
    defaultMode: "development",
    development: {
        server: {
            port: 3002,
            logFile: "dev_events.log",
            logFileOptions : {flags : 'w', encoding: 'utf8', name: 'dev_events.xml'},
            tempDataFileOptions : {flags : 'w', encoding: 'utf8', temp: 'temp/temp_category', origin: 'results/category'}, // temp_category_<id>.yml, category_<id>.yml
            keepAliveAgent: {
                maxSockets: 100,
                maxFreeSockets: 10,
                timeout: 60000,
                keepAliveTimeout: 30000
            }
        },

        db: {
            "username": "yagubov",
            "password": "",
            "database": "microservice",
            "host": "192.168.13.33",
            "port": "5432",
            "dialect": "postgres"
        }
    },
    production: {
        server: {
            port: 3002,
            logFile: "prod_events.log",
            logFileOptions : {flags : 'w', encoding: 'utf8', name: 'dev_events.xml'},
            tempDataFileOptions : {flags : 'w', encoding: 'utf8', temp: 'temp/temp_category', origin: 'results/category'}, // temp_category_<id>.yml, category_<id>.yml
            keepAliveAgent: {
                maxSockets: 100,
                maxFreeSockets: 10,
                timeout: 60000,
                keepAliveTimeout: 30000
            }
        },

        db: {
            "username": "service",
            "password": "rA7Xu39Kr85d4WH",
            "database": "microservice",
            "host": "localhost",
            "port": "5432",
            "dialect": "postgres"
        }
    }
};
