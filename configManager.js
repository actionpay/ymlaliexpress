var config = require('./config.js').config;

exports.getConfig = function(mode) {
    var mode = (mode != undefined) ? mode
        : config.hasOwnProperty("defaultMode") ? config.defaultMode  : 'test';

    if (config.hasOwnProperty(mode)) {
        return config[mode];
    } else {
        return null;
    }
};