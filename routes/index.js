var express = require('express');
var http = require('http');
var pull = require('../pull.js');
var router = express.Router();



/* GET home page. */
router.get('/', function(req, res, next) {
    pull.pullAll();
    res.send();
});

module.exports = router;
